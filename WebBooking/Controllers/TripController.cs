﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using WebBooking.Entity;
using WebBooking.Models;
using System.Data.Entity.Validation;

namespace WebBooking.Controllers
{
    public class TripController : Controller
    {
        private BookingRecordEntities2 db = new BookingRecordEntities2();

        // GET: Trip
        public ActionResult Index()
        {
            return View(db.Trips.ToList());
        }

        [HttpGet]
        public ActionResult New(int? id,Trip t=null)
        {
            Trip tr = new Trip();
            if (t != null)
                tr = t;
            var br = db.BookingRecords.Where(x => x.BookingNo == id);
            if (br.ToArray().Length > 0)
            {
                tr.TripNo += 1;
                tr.BookingNo = br.ToArray()[0].BookingNo;
                ViewBag.License = this.Car_licent();
                ViewBag.Type_car = this.get_Type_car();
                ViewBag.Own_car = this.get_Own_car();
                ViewBag.Chauffeur = this.get_Chauffeur();
                ViewBag.br_list = br.ToList();
                ViewBag.br_arr = br.FirstOrDefault<BookingRecord>();
                return View(tr);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult New(Trip t)
        {
            try
            {
                db.Trips.Add(t);
                db.SaveChanges();
                return Redirect("/Trip/Details/" + t.TripNo);
            }
            catch (DbEntityValidationException e)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest); // ส่งหน้า error 
            }
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            var trip_de = db.Trips.Where(x => x.TripNo == id);
            if (trip_de.ToArray().Length > 0)
                return View(trip_de.First<Trip>());
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        public ActionResult Copy(int? id)
        {
            var br = db.Trips.Where(x => x.TripNo == id).FirstOrDefault<Trip>();
            return this.New(br.BookingNo,br);
        }

        [HttpPost]
        public ActionResult Copy(Trip t)
        {
            return this.New(t);
        }

        public List<Trip_model> Car_licent()
        {
            return new List<Trip_model>()
            {
                new Trip_model()
                {
                    License_plate="asdasd"
                }
            };
        }

        public List<Trip_model> get_Type_car()
        {
            return new List<Trip_model>()
            {
                new Trip_model()
                {
                    Type_car="1"
                }
            };
        }

        public List<Trip_model> get_Chauffeur()
        {
            return new List<Trip_model>()
            {
                new Trip_model()
                {
                    Chauffeur="1"
                }
            };
        }

        public List<Trip_model> get_Own_car()
        {
            return new List<Trip_model>()
            {
                new Trip_model()
                {
                    Own_car="1"
                }
            };
        }

        public List<BookingRecord> get_CodeStart()
        {
            return new List<BookingRecord>()
            {
                new BookingRecord()
                {
                    CodeStart=10
                }
            };
        }

        public List<BookingRecord> get_CodeEnd()
        {
            return new List<BookingRecord>()
            {
                new BookingRecord()
                {
                    CodeEnd=12456
                }
            };
        }
    }
}