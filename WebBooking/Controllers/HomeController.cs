﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBooking.Models;

namespace WebBooking.Controllers
{
    public class HomeController : Controller
    {
        BookingRepository repo = new BookingRepository();
        public ActionResult Index()
        {
            var result = repo.getAllRecord();
            return View(result);
        }
        
        [HttpGet]
        public ActionResult Edit(BookingModel id)
        {
            return View(id);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(BookingModel record)
        {
            repo.AddnewBooking(record);
            return Redirect("/Home/Index");
        }
    }
}