﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBooking.Models
{
    public class BookingModel
    {
        public int BookingNo { get; set; }
        public string JobType { get; set; }
        public int CustomerID { get; set; }
        public int LoanCustomer { get; set; }
        public string OpenTrip { get; set; }
        public string BillAdd { get; set; }
        public string TransportType { get; set; }
        public string ProductType { get; set; }
        public int StartCode { get; set; }
        public int EndCode { get; set; }
        public DateTime SendDate { get; set; }
        public int Weigth { get; set; }
        public int Value { get; set; }
        public DateTime Postdate { get; set; }
        public int CreditTerm { get; set; }
        public string WeightType { get; set; }
        public string ValueType { get; set; }
    }
}