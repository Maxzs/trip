﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBooking.Entity;
namespace WebBooking.Models
{
    public class BookingRepository
    {
        BookingRecordEntities3 db = new BookingRecordEntities3();

        public IEnumerable<BookingRecord> getAllRecord()
        {
            var result = db.BookingRecords;
            return result;
        }

        public void AddnewBooking(BookingModel record)
        {
            BookingRecord newRecord = new BookingRecord();

            newRecord.Amount = record.Value;
            newRecord.BillAddress = record.BillAdd;
            newRecord.BookingNo = record.BookingNo;
            newRecord.CodeEnd = record.EndCode;
            newRecord.CodeStart = record.StartCode;
            newRecord.CreditTerm = record.CreditTerm;
            newRecord.CustomerID = record.CustomerID;
            newRecord.Date = record.Postdate;
            newRecord.DateTranspot = record.SendDate;
            newRecord.JobType = record.JobType;
            newRecord.LoanCustomer = record.LoanCustomer;
            newRecord.ProductType = record.ProductType;
            newRecord.TranspotType = record.TransportType;
            newRecord.TripOpen = record.OpenTrip;
            newRecord.Weigth = record.Weigth;
            db.BookingRecords.Add(newRecord);
            db.SaveChanges();
        }

        public object FindBooking(int Booking_ID=0)
        {
            var result =  db.BookingRecords.Single(x => x.BookingNo == Booking_ID);
            return result;
        }
    }
}