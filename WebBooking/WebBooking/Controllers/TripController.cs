﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBooking.Models;

namespace WebBooking.Controllers
{
    public class TripController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult New()
        {
            BookingRepository re = new BookingRepository();
            int booking_ID = Convert.ToInt32(HttpContext.Request.Form["booking_id"]);
            return View(re.FindBooking(booking_ID));
        }
    }
}