﻿namespace WebBooking.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using WebBooking.Models;

    public partial class Trip_model
    {
        public int BookingNo { get; set; }

        public string TripNo   { get; set; }
        [Key]

        public string License_plate  { get; set; }
        public string Chauffeur { get; set; }
        public string Type_car { get; set; }
        public string Own_car  { get; set; }
        public string Source  { get; set; }
        public string Destination  { get; set; }
        public float Range { get; set; }
        public string  Note { get; set; }
        public string Product_code { get; set; }
        public int Num_way  { get; set; }
        public int Lost_count   { get; set; }
        public int Corrupt_count   { get; set; }        
        public float Weight_start   { get; set; }
        public float Weight_end   { get; set; }
        public float Weight_lost   { get; set; }
        public float Weight_corrupt   { get; set; }
        public int Count_last { get; set; }
    }
}