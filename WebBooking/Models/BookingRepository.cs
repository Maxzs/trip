﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBooking.Entity;
namespace WebBooking.Models
{
    public class BookingRepository
    {
        BookingRecordEntities2 db = new BookingRecordEntities2();
        public IEnumerable<BookingRecord> getAllRecord()
        {
            var result = db.BookingRecords;
            return result;
        }

        public void AddnewBooking(BookingModel record)
        {
            BookingRecord newRecord = new BookingRecord();
            
            newRecord.Amount = record.Value;
            newRecord.BillAddress = record.BillAddress;
            newRecord.BookingNo = record.BookingNo;
            newRecord.CodeEnd = record.EndCode;
            newRecord.CodeStart = record.StartCode;
            newRecord.CreditTerm = record.CreditTerm;
            newRecord.CustomerID = record.CustomerID;
            newRecord.Date = record.Postdate;
            newRecord.DateTranspot = record.DateTranspot;
            newRecord.JobType = record.JobType;
            newRecord.LoanCustomer = record.LoanCustomer;
            newRecord.ProductType = record.ProductType;
            newRecord.TranspotType = record.TransportType;
            newRecord.TripOpen = record.TripOpen;
            newRecord.Weigth = record.Weigth;
            db.BookingRecords.Add(newRecord);
            db.SaveChanges();
        }

    }
}